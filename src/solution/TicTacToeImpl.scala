package solution

import java.io.FileInputStream
import java.util
import java.util.Optional

import alice._
import alice.tuprolog.{Struct, Theory}
import solution.Player.{PlayerO, PlayerX}
import solution.Scala2P._

import scala.collection.JavaConverters._
import scala.collection.mutable.Buffer

/**
  * Created by mirko on 4/10/17.
  */
class TicTacToeImpl(fileName: String) extends TicTacToe {

  implicit private def playerToString(player: Player): String = player match {
    case PlayerX => "p1"
    case _ => "p2"
  }

  implicit private def stringToPlayer(s: String): Player = s match {
    case "p1" => PlayerX
    case _ => Player.PlayerO
  }

  implicit private def boardToString(b: util.List[Optional[Player]]): String = {
    var board: String = "["

    for (i <- 0 until b.size ()) {
      if (b.get (i).equals (Optional.empty [Player]())) {
        board = board + "null,"
      } else {
        b.get (i).get () match {
          case PlayerX => board = board + "p1,"
          case PlayerO => board = board + "p2,"
        }
      }
    }

    board = board take (board.length - 1)
    board = board + "]"
    board
  }

  private val engine = mkPrologEngine (new Theory (new FileInputStream (fileName)))

  createBoard ()

  override def createBoard(): Unit = {
    val goal = "retractall(board(_)),create_board(B),assert(board(B))"
    solveWithSuccess (engine, goal)
  }

  override def getBoard: util.List[Optional[Player]] = {
    val term = solveOneAndGetTerm (engine, "board(B)", "B").asInstanceOf [Struct]
    val iterator = term.listIterator ()
    iterator.asScala.toList.map (_.toString).map {
      case "null" => Optional.empty [Player]()
      case s => Optional.of [Player](s)
    }.to [Buffer].asJava
  }

  override def checkCompleted(): Boolean = {
    val goal = "board(B),final(B,_)"
    solveWithSuccess (engine, goal)
  }

  override def checkVictory(): Optional[Player] = {
    val goal = "board(B),final(B,p1)"
    val goal2 = "board(B),final(B,p2)"
    if (solveWithSuccess (engine, goal)) Optional.of (PlayerX)
    else if (solveWithSuccess (engine, goal2)) Optional.of (Player.PlayerO)
    else Optional.empty ()
  }

  override def move(player: Player, i: Int, j: Int): Boolean = {
    val goal = s"board(B), next_board(B,${playerToString (player)},B2)"
    val nextboard = (for {
      term <- engine (goal).map (extractTerm (_, "B2"))
      elem = term.asInstanceOf [Struct].listIterator ().asScala.toList (i + 3 * j)
      if elem.toString == playerToString (player)
    } yield term).headOption
    if (nextboard isEmpty) return false
    val goal2 = s"retractall(board(_)), assert(board(${nextboard.get.toString}))"
    solveWithSuccess (engine, goal2)
  }

  override def toString: String = solveOneAndGetTerm (engine, "board(B)", "B").toString

  override def winCount(current: Player, winner: Player): Int = {
    val goal = s"board(B), statistics(B,${playerToString (current)},${playerToString (winner)},Count)"
    solveOneAndGetTerm (engine, goal, "Count").asInstanceOf [tuprolog.Int].intValue ()
  }

  private def isChecked(i: Int): Boolean = solveWithSuccess (engine, s"board(B), check_corner(B, $i, ${playerToString (PlayerO)})")

  private def isFree(i: Int): Boolean = solveWithSuccess (engine, s"board(B), isFree(B, $i)")

  private def findVictoryOnRows(player: Player): Boolean = solveWithSuccess (engine, s"board(B), check_row(B,${playerToString (player)})")

  private def findVictoryOnColumns(player: Player): Boolean = solveWithSuccess (engine, s"board(B), check_column(B,${playerToString (player)})")

  private def findVictoryOnDiagonals(player: Player): Boolean = solveWithSuccess (engine, s"board(B), check_diagonal(B,${playerToString (player)})")

  private def getPositionOnColumns(player: Player): Position = {
    val goal = s"board(B), getPosition(B,[1,2,3,4,5,6,7,8,9],${playerToString (player)},Index)"
    val res = solveOneAndGetTerm (engine, goal, "Index").asInstanceOf [tuprolog.Int].intValue ()
    mapIndexToCoordinate (res)
  }

  private def getPositionOnRows(player: Player): Position = {
    val goal = s"board(B), getPosition(B,[1,4,7,2,5,8,3,6,9],${playerToString (player)},Index)"
    val res = solveOneAndGetTerm (engine, goal, "Index").asInstanceOf [tuprolog.Int].intValue ()
    mapIndexToCoordinate (res)
  }

  private def getPositionOnDiagonals(player: Player): Position = {
    val goal = s"board(B), getPosition(B,[1,5,9,3,5,7,_,_,_],${playerToString (player)},Index)"
    val res = solveOneAndGetTerm (engine, goal, "Index").asInstanceOf [tuprolog.Int].intValue ()
    mapIndexToCoordinate (res)
  }

  private def mapIndexToCoordinate(i: Int): Position = {
    i match {
      case 1 => new Position (0, 0)
      case 2 => new Position (1, 0)
      case 3 => new Position (2, 0)
      case 4 => new Position (0, 1)
      case 5 => new Position (1, 1)
      case 6 => new Position (2, 1)
      case 7 => new Position (0, 2)
      case 8 => new Position (1, 2)
      case 9 => new Position (2, 2)
    }
  }

  private def minimax(): Position = {
    val testBoard = getBoard
    var position = new Position (0, 0)
    var assigned = false
    var computerWin = 0

    for (i <- 0 until testBoard.size ()) {

      //If i discover a free space
      if (isFree (i + 1)) {

        //Get the position
        val pos = mapIndexToCoordinate (i + 1)

        //Setto la mossa
        testBoard.set (i, Optional.of (Player.PlayerO))

        //Calcolo le chance
        val goalComputer = s"statistics(${boardToString (testBoard)},${playerToString (PlayerX)},${playerToString (PlayerO)},Count)"
        val goalHuman = s"statistics(${boardToString (testBoard)},${playerToString (PlayerX)},${playerToString (PlayerX)},Count)"

        val newComputerWin = solveOneAndGetTerm (engine, goalComputer, "Count").asInstanceOf [tuprolog.Int].intValue ()
        val humanWin = solveOneAndGetTerm (engine, goalHuman, "Count").asInstanceOf [tuprolog.Int].intValue ()

        //Controllo se ho una miglioria
        if (newComputerWin >= humanWin && newComputerWin >= computerWin) {
          assigned = true
          position = new Position (pos.getI, pos.getJ)
          computerWin = newComputerWin
        }

        //Resetto la board
        testBoard.set (i, Optional.empty ())
      }
    }

    if (!assigned) {
      for (i <- 0 until testBoard.size ()) {
        if (isFree (i + 1))
          position = mapIndexToCoordinate (i + 1)
      }
    }

    position
  }

  private def triangulation(): Position = {
    //Angolo alto SX
    if (isChecked (1)) {
      if (isChecked (3) && isFree (7)) {
        return mapIndexToCoordinate (7)
      }

      if (isChecked (7) && isFree (3)) {
        return mapIndexToCoordinate (3)
      }

      if (isFree (3)) {
        return mapIndexToCoordinate (3)
      }

      if (isFree (7)) {
        return mapIndexToCoordinate (7)
      }
    }

    //Angolo basso SX
    if (isChecked (3)) {
      if (isChecked (1) && isFree (9)) {
        return mapIndexToCoordinate (9)
      }

      if (isChecked (9) && isFree (1)) {
        return mapIndexToCoordinate (1)
      }

      if (isFree (1)) {
        return mapIndexToCoordinate (1)
      }

      if (isFree (9)) {
        return mapIndexToCoordinate (9)
      }
    }

    //Angolo alto DX
    if (isChecked (7)) {
      if (isChecked (1) && isFree (9)) {
        return mapIndexToCoordinate (9)
      }

      if (isChecked (9) && isFree (1)) {
        return mapIndexToCoordinate (1)
      }

      if (isFree (1)) {
        return mapIndexToCoordinate (1)
      }

      if (isFree (9)) {
        return mapIndexToCoordinate (9)
      }
    }

    //Angolo basso a DX
    if (isChecked (9)) {
      if (isChecked (3) && isFree (7)) {
        return mapIndexToCoordinate (7)
      }

      if (isChecked (7) && isFree (3)) {
        return mapIndexToCoordinate (3)
      }

      if (isFree (3)) {
        return mapIndexToCoordinate (3)
      }

      if (isFree (7)) {
        return mapIndexToCoordinate (7)
      }
    }

    null
  }

  override def nextMove(current: Player, winner: Player): Position = {
    //First I try to win
    val win = checkBoard (PlayerO)
    if (win != null)
      win
    else {
      //Then I try to stop human
      val stop = checkBoard (PlayerX)
      if (stop != null)
        stop
      else {
        val tr = triangulation ()
        if (tr != null) {
          tr
        } else {
          minimax ()
        }
      }
    }
  }

  private def checkBoard(player: Player): Position = {
    if (findVictoryOnRows (player)) {
      getPositionOnRows (player)
    } else {
      if (findVictoryOnColumns (player)) {
        getPositionOnColumns (player)
      } else {
        if (findVictoryOnDiagonals (player)) {
          getPositionOnDiagonals (player)
        } else {
          null
        }
      }
    }
  }
}
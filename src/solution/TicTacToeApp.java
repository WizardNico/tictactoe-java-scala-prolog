package solution;


import javax.swing.*;
import java.awt.*;
import java.util.Optional;

/**
 * A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private final TicTacToe ttt;
    private final JButton[][] board = new JButton[3][3];
    private final JButton exit = new JButton("Exit");
    private final JButton startComputer = new JButton("Computer-First");
    private final JFrame frame = new JFrame("TTT");
    private boolean finished = false;
    private Player turn = Player.PlayerX;
    private int moves = 0;

    private void changeTurn() {
        this.turn = this.turn == Player.PlayerX ? Player.PlayerO : Player.PlayerX;
    }

    public TicTacToeApp(TicTacToe ttt) throws Exception {
        this.ttt = ttt;
        initPane();
    }

    //Take an angle
    private Position firstMove() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if ((i == 0 && j == 0) || (i == 2 && j == 0) || (i == 0 && j == 2) || (i == 2 && j == 2)) {
                    if (board[i][j].isEnabled()) {
                        return (new Position(i, j));
                    }
                }
            }
        }
        return null;
    }

    private void checkBox(Position position, String symbol) {
        board[position.getI()][position.getJ()].setText(symbol);
        board[position.getI()][position.getJ()].setEnabled(false);
        moves++;
    }

    private void computerMove() {
        System.out.println("[COMPUTER] I'm thinking....");

        if (moves < 2) {
            Position position = firstMove();
            assert position != null;
            if (ttt.move(turn, position.getI(), position.getJ())) {
                checkBox(position, "O");
                changeTurn();
            }
        } else {
            Position pos = ttt.nextMove(turn, Player.PlayerO);

            if (ttt.move(turn, pos.getI(), pos.getJ())) {
                checkBox(pos, "O");
                changeTurn();
                System.out.println("[COMPUTER] moves to win: " + ttt.winCount(turn, Player.PlayerO));
                System.out.println("[HUMAN] moves to win: " + ttt.winCount(turn, Player.PlayerX));
            }
        }
        checkState();

        System.out.println("[COMPUTER] Ok, thake this!");
    }

    private void humanMove(Position position) {
        if (ttt.move(turn, position.getI(), position.getJ())) {
            checkBox(position, "X");
            changeTurn();
        }
        checkState();
    }

    private void checkState() {
        Optional<Player> victory = ttt.checkVictory();
        if (victory.isPresent()) {
            exit.setText(victory.get() + " won!");
            finished = true;
            return;
        }

        if (ttt.checkCompleted()) {
            exit.setText("Even!");
            finished = true;
        }
    }

    private void initPane() {
        frame.setLayout(new BorderLayout());
        JPanel b = new JPanel(new GridLayout(3, 3));
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                final int i2 = i;
                final int j2 = j;
                board[i][j] = new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> {
                    if (!finished) {
                        startComputer.setEnabled(false);
                        humanMove(new Position(i2, j2));//Human make movement
                        if (!finished) {
                            computerMove();//Computer compute a real time response to the human movement
                        }
                    }
                });
            }
        }
        JPanel s = new JPanel(new FlowLayout());
        s.add(exit);
        s.add(startComputer);
        exit.addActionListener(e -> System.exit(0));
        startComputer.addActionListener(e -> {
            changeTurn();
            computerMove();
            startComputer.setEnabled(false);
        });
        frame.add(BorderLayout.CENTER, b);
        frame.add(BorderLayout.SOUTH, s);
        frame.setSize(400, 430);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        try {
            new TicTacToeApp(new TicTacToeImpl("src/solution/ttt.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}

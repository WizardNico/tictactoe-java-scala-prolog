package solution;

import java.util.Optional;

enum Player {PlayerX, PlayerO}

public interface TicTacToe {

    void createBoard();

    java.util.List<Optional<Player>> getBoard();

    boolean checkCompleted();

    Optional<Player> checkVictory();

    boolean move(Player player, int i, int j);

    int winCount(Player current, Player winner);

    Position nextMove(Player current, Player winner);
}

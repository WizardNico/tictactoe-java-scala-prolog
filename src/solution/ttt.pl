% symbol(+OnBoard,-OnScreen): symbols for the board and their rendering
symbol(null,'_').
symbol(p1,'X').
symbol(p2,'O').

% result(+OnBoard,-OnScreen): result of a game
result(even,"even!").
result(p1,"player 1 wins").
result(p2,"player 2 wins").

% other_player(?Player,?OtherPlayer)
other_player(p1,p2).
other_player(p2,p1).

% render(+List): prints a TTT table (9 elements list) on console
render(L) :- convert_symbols(L,[A,B,C,D,E,F,G,H,I]),print_row(A,B,C),print_row(D,E,F),print_row(G,H,I).
convert_symbols(L,L2) :- findall(R,(member(X,L),symbol(X,R)),L2).
print_row(A,B,C) :- put(A),put(' '),put(B),put(' '),put(C),nl.

% render(+List,+Result): prints a TTT table plus result
render_full(L,Result) :- result(Result,OnScreen),print(OnScreen),nl,render(L),nl,nl.

% create_board(-Board): creates an initially empty board
create_board(B):-create_list(9,null,B).
create_list(0,_,[]) :- !.
create_list(N,X,[X|T]) :- N2 is N-1, create_list(N2,X,T).

% next_board(+Board,+Player,?NewBoard): finds (zero, one or many) new boards as Player moves
next_board([null|B],PL,[PL|B]).
next_board([X|B],PL,[X|B2]):-next_board(B,PL,B2).

% final(+Board,-Result): checks where the board is final and why
final(B,p1) :- finalpatt(P), match(B,P,p1),!.
final(B,p2) :- finalpatt(P), match(B,P,p2),!.
final(B,even) :- not(member(null,B)).

% match(Board,Pattern,Player): checks if in the board, the player matches a winning pattern
match([],[],_).
match([M|B],[x|P],M):-match(B,P,M).
match([_|B],[o|P],M):-match(B,P,M).

% finalpatt(+Pattern): gives a winning pattern
finalpatt([x,x,x,o,o,o,o,o,o]).
finalpatt([o,o,o,x,x,x,o,o,o]).
finalpatt([o,o,o,o,o,o,x,x,x]).
finalpatt([x,o,o,x,o,o,x,o,o]).
finalpatt([o,x,o,o,x,o,o,x,o]).
finalpatt([o,o,x,o,o,x,o,o,x]).
finalpatt([x,o,o,o,x,o,o,o,x]).
finalpatt([o,o,x,o,x,o,x,o,o]).

% game(+Board,+Player,-FinalBoard,-Result): finds one (zero, one or many) final boards and results
game(B,_,B,Result) :- final(B,Result),!.
game(B,PL,BF,Result):- next_board(B,PL,B2), other_player(PL,PL2),game(B2,PL2,BF,Result).

% statistics(+Board,+Player,+Result,-Count): counts how many time Res will happen 
statistics(B,P,Res,Count) :- findall(a, game(B,P,_,Res),L), length(L,Count).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---IMPLEMENTED---%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% listFinalBoards(+Board,+Player,+Result,-List): get the list of final boards
listFinalBoards(B,P,Res,L) :- findall(RB,game(B,P,RB,Res),L).

%isFree(+Board,+Square,-{True, False}): check if the square is free
isFree(Board, C) :- checkFreeSquare(Board, C), !.

%check_corner(+Board,+Square,+Symbol{p1=X,p2=O},-{True, False}):
%check if a corner is filled with Symbol (p2 for example to start triangulation) to start triangulation.
%possible value for Square = {1=TOP_LEFT, 7=TOP_RIGHT, 3=BOTTOM_LEFT, 9=BOTTOM_RIGHT}
check_corner(Board, C, P) :- checkSquare(Board, C, P), !.

%check_row(+Board,+Symbol{p1=X,p2=O},-{True, False}): check all row to find an immediate victory
%Symbol = p1, an immediate vicotry for the opponent, that we have to stop;
%Symbol = p2, an immediate vicotry for the computer, that we have to fill the square to win;
check_row(Board,P) :- findVictory(Board, [1, 4, 7], P), !.
check_row(Board,P) :- findVictory(Board, [2, 5, 8], P), !.
check_row(Board,P) :- findVictory(Board, [3, 6, 9], P), !.

%check_column(+Board,+Symbol{p1=X,p2=O},-{True, False}): check all columns to find an immediate victory
%Symbol = p1, an immediate vicotry for the opponent, that we have to stop;
%Symbol = p2, an immediate vicotry for the computer, that we have to fill the square to win;
check_column(Board,P) :- findVictory(Board, [1, 2, 3], P), !.
check_column(Board,P) :- findVictory(Board, [4, 5, 6], P), !.
check_column(Board,P) :- findVictory(Board, [7, 8, 9], P), !.

%check_diagonal(+Board,+Symbol{p1=X,p2=O},-{True, False}): check all diagonal to find an immediate victory
%Symbol = p1, an immediate vicotry for the opponent, that we have to stop;
%Symbol = p2, an immediate vicotry for the computer, that we have to fill the square to win;
check_diagonal(Board,P) :- findVictory(Board, [1, 5, 9], P), !.
check_diagonal(Board,P) :- findVictory(Board, [3, 5, 7], P), !.

%findVictory(+Board,+[index1,index2,index3],+Symbol{p1=X,p2=O},-{True, False})
%try all combination for all indexes, to find 2 square filled with the same symbol and the 3th square free for an immediate victory
findVictory(Board, [S1, S2, S3], P) :-
  checkFreeSquare(Board, S1),
  checkSquare(Board, S2, P),
  checkSquare(Board, S3, P);
       checkSquare(Board, S1, P),
       checkFreeSquare(Board, S2),
       checkSquare(Board, S3,P);
            checkSquare(Board, S1, P),
            checkSquare(Board, S2, P),
            checkFreeSquare(Board, S3).

%getPosition(+Board,+ListOfIndexes,+Symbol{p1=X,p2=O},-Index)
%Allow to get the position to fill: with symbol p1, to stop enemy otherwise with p2 to win
%List could be: [1,2,3,4,5,6,7,8,9] -> to check columns || [1,4,7,2,5,8,3,6,9] -> to check rows || [1,5,9,3,5,7,_,_,_] - > to check diagonals
getPosition(Board, [S1, S2, S3, S4, S5, S6, S7, S8, S9], P, Pos) :-
  checkFreeSquare(Board, S1),Pos=S1,
  checkSquare(Board, S2, P),
  checkSquare(Board, S3, P),!;
  	checkSquare(Board, S1, P),
  	checkFreeSquare(Board, S2), Pos=S2,
  	checkSquare(Board, S3, P),!;
    	checkSquare(Board, S1, P),
  		checkSquare(Board, S2, P),
  		checkFreeSquare(Board, S3), Pos=S3,!;

  checkFreeSquare(Board, S4),Pos=S4,
  checkSquare(Board, S5, P),
  checkSquare(Board, S6, P),!;
  	checkSquare(Board, S4, P),
  	checkFreeSquare(Board, S5), Pos=S5,
  	checkSquare(Board, S6, P),!;
    	checkSquare(Board, S4, P),
  		checkSquare(Board, S5, P),
  		checkFreeSquare(Board, S6), Pos=S6, !;

  checkFreeSquare(Board, S7),Pos=S7,
  checkSquare(Board, S8, P),
  checkSquare(Board, S9, P),!;
  	checkSquare(Board, S7, P),
  	checkFreeSquare(Board, S8), Pos=S8,
  	checkSquare(Board, S9, P),!;
    	checkSquare(Board, S7, P),
  		checkSquare(Board, S8, P),
  		checkFreeSquare(Board, S9), Pos=S9.

% checkSquare(+Board, +Index, +Symbol, -{True, False}) : check if any i-th element is a P
checkSquare([P, _, _, _, _, _, _, _, _], 1, P).
checkSquare([_, P, _, _, _, _, _, _, _], 2, P).
checkSquare([_, _, P, _, _, _, _, _, _], 3, P).
checkSquare([_, _, _, P, _, _, _, _, _], 4, P).
checkSquare([_, _, _, _, P, _, _, _, _], 5, P).
checkSquare([_, _, _, _, _, P, _, _, _], 6, P).
checkSquare([_, _, _, _, _, _, P, _, _], 7, P).
checkSquare([_, _, _, _, _, _, _, P, _], 8, P).
checkSquare([_, _, _, _, _, _, _, _, P], 9, P).

% checkFreeSquare(+Board, +Index, -{True, False}) : check if any i-th element is free
checkFreeSquare([null, _, _, _, _, _, _, _, _], 1).
checkFreeSquare([_, null, _, _, _, _, _, _, _], 2).
checkFreeSquare([_, _, null, _, _, _, _, _, _], 3).
checkFreeSquare([_, _, _, null, _, _, _, _, _], 4).
checkFreeSquare([_, _, _, _, null, _, _, _, _], 5).
checkFreeSquare([_, _, _, _, _, null, _, _, _], 6).
checkFreeSquare([_, _, _, _, _, _, null, _, _], 7).
checkFreeSquare([_, _, _, _, _, _, _, null, _], 8).
checkFreeSquare([_, _, _, _, _, _, _, _, null], 9).